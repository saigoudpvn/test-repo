package main

func main() {
	cards := deck{"hi", "hello", newWorld(), nextWorld()}
	cards = append(cards, "Thank you")

	cards.print()

}

func newWorld() string {
	return "bye"
}

func nextWorld() string {
	return "Good bye"
}
